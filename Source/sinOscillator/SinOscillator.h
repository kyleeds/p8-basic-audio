/*
 *  SinOscillator.h
 *  sdaAudioMidi
 *
 *  Created by tjmitche on 11/11/2010.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef H_SINOSCILLATOR
#define H_SINOSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"
#include "../oscillator/Oscillator.hpp"

/**
 Class for a sine oscillator.
 @see Oscillator
 */

class SinOscillator  : public Oscillator
{
public:
	/**SinOscillator constructor*/
	SinOscillator();
	
	/**SinOscillator destructor*/
	virtual ~SinOscillator();
    
    /** function that provides the execution of the waveshape 
     @param float currentPhase is the phase position of the waveform and it's usually between 0 and 2*M_PI
     @return returns a float amplitude value of the waveform at the current phase position. As this is a sine wave, it will be between -1 and +1 */
	virtual float renderWaveShape (const float currentPhase) override;
	
private:

};

#endif //H_SINOSCILLATOR