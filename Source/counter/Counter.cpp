//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 10/11/2016.
//
//

#include "Counter.hpp"

Counter::Counter() : Thread ("CounterThread")
{
    
    listener = nullptr;
    counter = 0;
    startThread();
    
}

Counter::~Counter()
{
    stopThread(500);
}
void Counter::startCounter()
{
    startThread();
}

void Counter::stopCounter()
{
    counter = 0;
    stopThread(500);
}

int Counter::nextCounter()
{
    counter++;
    return counter;
}

void Counter::setBPM(int newBPM)
{
    bpm.set(newBPM);
}

void Counter::run()
{
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        if (listener != nullptr)
        {
            listener->counterChanged (counter++);
        }
        Time::waitForMillisecondCounter(time + (60*1000/bpm.get()));
    }
}

void Counter::setListener(Listener* newListener)
{
    listener = newListener;
}