//
//  Counter.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 10/11/2016.
//
//

#ifndef Counter_hpp
#define Counter_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

/**
 Class for a counter
 */

class Counter : public Thread
{
public:
    /** Constructor */
    Counter();
    
    /** Destructor */
    ~Counter();
    
    /** Function that starts the counter */
    void startCounter();
    
    /** Function that stops the counter */
    void stopCounter();
    
    /** Function that return the next counter value 
     @return returns the next integer counter value*/
    int nextCounter();
    
    /** Function that sets the Beats Per Minute value of the counter
     @param int newBPM is beats per minute usually ranging from 10 - 800 for an advanced metronome. */
    void setBPM(int newBPM);
    
    /** Function that runs the counter */
    void run() override;
    
    
    /**Class for counter listeners to inherit */
    class Listener
    {
        public:
        /** Destructor. */
        virtual ~Listener() {}
        
        /** This pure function is called when the next timer has reached the next interval.
         @param const unsigned int counterValue is the current counter value that you could use to print out or to process osmething else.*/
        virtual void counterChanged (const unsigned int counterValue) = 0;
    };
    
    /** Function that sets the counter listener to a specific counter object
     @param newListener is to set the listener*/
    void setListener(Listener* newListener);
    
private:
    uint32 counter;
    Listener* listener;
    Atomic<float> bpm;
    
};


#endif /* Counter_hpp */
