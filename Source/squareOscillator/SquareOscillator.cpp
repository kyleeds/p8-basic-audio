//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 10/11/2016.
//
//

#include "SquareOscillator.hpp"
#include <cmath>

SquareOscillator::SquareOscillator()
{
    setDutyCycle (50);
}
SquareOscillator::~SquareOscillator()
{
    
}

float SquareOscillator::renderWaveShape(const float currentPhase)
{
    if (currentPhase <= dutyCycle)
    {
        return 1.f;
    }
    else
    {
        return -1.f;
    }
}

void SquareOscillator::setDutyCycle(float newDutyCycle)
{
    dutyCycle = newDutyCycle/100.f * 2*M_PI;
    //std::cout << dutyCycle << "\n";
}