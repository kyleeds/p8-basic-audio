//
//  SquareOscillator.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 10/11/2016.
//
//

#ifndef H_SQUAREOSCILLATOR
#define H_SQUAREOSCILLATOR

#include <stdio.h>
#include "../oscillator/Oscillator.hpp"

/**
 Class for a square wave oscillator. 
 @see Oscillator class for base class info
 @see Oscillator
 */

class SquareOscillator  : public Oscillator

{
public:
    /**Oscillator constructor*/
    SquareOscillator();
    
    /**Oscillator destructor*/
    virtual ~SquareOscillator();
    
    /** function that provides the execution of the waveshape 
     @param float currentPhase is the phase position of the waveform and it's usually between 0 and 2*M_PI 
     @return returns a float amplitude value of the waveform at the current phase position. As this is a quare wave, it will be either -1 or +1 */
    float renderWaveShape(const float currentPhase) override;
    
    /** function that sets the duty cycle of the square wave 
     @param float newDutyCycle is the Duty Cycle value in percentage between 0% and 100% */
    void setDutyCycle(float newDutyCycle);

private:
    float dutyCycle;
    
};

#endif //H_SQUAREOSCILLATOR
