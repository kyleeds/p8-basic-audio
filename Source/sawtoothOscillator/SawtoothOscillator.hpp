//
//  SawtoothOscillator.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 11/11/2016.
//
//

//#ifndef SawtoothOscillator_hpp
//#define SawtoothOscillator_hpp
#ifndef H_SAWTOOTHOSCILLATOR
#define H_SAWTOOTHOSCILLATOR

#include <stdio.h>
#include "../oscillator/Oscillator.hpp"

/**
 Class for a sawtooth wave oscillator. 
 @see Oscillator class for base class info
 @see Oscillator
 */

class SawtoothOscillator  : public Oscillator

{
public:
    /**Oscillator constructor*/
    SawtoothOscillator();
    
    /**Oscillator destructor*/
    virtual ~SawtoothOscillator();
    
    /** function that provides the execution of the waveshape
     @param float currentPhase is the phase position of the waveform and it's usually between 0 and 2*M_PI
     @return returns a float amplitude value of the waveform at the current phase position. As this is a sawtooth wave, it will be between -1 and +1 */
    float renderWaveShape(const float currentPhase) override;
    
private:

    
};


#endif //H_SAWTOOTHOSCILLATOR
//#endif /* SawtoothOscillator_hpp */
