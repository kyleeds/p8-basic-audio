//
//  SawtoothOscillator.cpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 11/11/2016.
//
//

#include "SawtoothOscillator.hpp"
#include <cmath>

SawtoothOscillator::SawtoothOscillator()
{
    //setDutyCycle (50);
}
SawtoothOscillator::~SawtoothOscillator()
{
    
}

float SawtoothOscillator::renderWaveShape(const float currentPhase)
{
    if (currentPhase <= M_PI)
    {
        return currentPhase / M_PI;
    }
    else
    {
        return (currentPhase/M_PI)-2;
    }
}
