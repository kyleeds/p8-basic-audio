/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    osc = &oscSquare;
    counter = 44100;
    
    audioDeviceManager.setMidiInputEnabled("Impulse  Impulse", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message_)
{
    DBG("Note Number:" << message_.getNoteNumber());
    if (message_.isNoteOn())
    {
        newFrequency.set (MidiMessage::getMidiNoteInHertz (message_.getNoteNumber()));
        osc->setAmplitude(message_.getFloatVelocity());
    }
    else if (message_.isNoteOff())
    {
        newFrequency.set (0.f);
        osc->setAmplitude(0.f);
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    osc->setFrequency (newFrequency.get());
    
    while(numSamples--)
    {
        if (counter < 4410) {
            counter++;
            if (counter == 4410) {
                osc->setAmplitude(0.f);
            }
        }
        
        const float sample = osc->nextSample();
        *outL = sample;
        *outR = sample;
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    oscSin.setSampleRate (device->getCurrentSampleRate());
    oscSquare.setSampleRate (device->getCurrentSampleRate());
    oscSaw.setSampleRate (device->getCurrentSampleRate());
}

void Audio::audioDeviceStopped()
{

}

void Audio::beep()
{
    newFrequency.set(440);
    counter = 0;
    osc->setAmplitude(1.f);
    
}

void Audio::setWaveform(int newWaveform)
{
    if (newWaveform == 1) {
        osc = &oscSin;
    }
    else if (newWaveform == 2)
    {
        osc = &oscSquare;
    }
    else
    {
        osc = &oscSaw;
    }
    
}