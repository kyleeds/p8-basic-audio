/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 @see SinOscillator
 @see SquareOscillator
 @see SawtoothOscillator
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../sinOscillator/SinOscillator.h"
#include "../squareOscillator/SquareOscillator.hpp"
#include "../sawtoothOscillator/SawtoothOscillator.hpp"

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    /** Function that handles incoming MIDI messages and does something with it such as 'noteOff' and 'noteOn' */
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    /** Function that handles audio data output at sample rate */
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    /** Function that initialises stuff when audio is about to start such as setting the sample rate for different oscillators */
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    /** Function that does something when audio stops */
    void audioDeviceStopped() override;
    
    /** Function that creates a 100ms 440Hz beep sound when called */
    void beep();
    
    /** Function that sets the waveform between Sine and Square 
     @param int newWaveform is the waveform ID from the selected ComboBox list so usually starting from 1 and ending at the number of items in the ComboBox list.*/
    void setWaveform(int newWaveform);
    
    /** Function that sets the gain level 
     @param float newGain is the gain value between 0 and 1 */
    void setGain(float newGain)
    {
        gainValue = newGain;
    }
    
    /** Function that sets the duty cycle 
     @param float newDuty is the new Duty Cycle value in percentage between 0% and 100% */
    void setDutyCycle (float newDuty)
    {
        oscSquare.setDutyCycle (newDuty);
    }
    
private:
    AudioDeviceManager audioDeviceManager;
    Atomic<float> gainValue;
    Atomic<float> newFrequency;
    SquareOscillator oscSquare;
    SinOscillator oscSin;
    SawtoothOscillator oscSaw;
    Oscillator* osc;
    int counter;
    
};



#endif  // AUDIO_H_INCLUDED
